# Readme #

This package is simply a user object which extends the `Zend\Permissions\Acl\Role\RoleInterface`. This allows you to pass the user object to the `isAllowed()` method when querying the ACL. You can also populate it miscellaneous data from your Authentication library or from some other source. 

See https://docs.zendframework.com/zend-permissions-acl/ for details on using Zend's ACL library.

## Example ##


```
#!php
<?php
$acl = new Zend\Permissions\Acl\Acl;
$acl->addRole('guest');
$acl->addRole('user', 'guest');
$acl->addRole('admin', 'user');
	
$acl->addResource('posts');
	
$acl->allow('guest', 'posts', 'view');
$acl->allow('user', 'posts', 'add');
$acl->allow('admin');

// User data, such as the role, username, etc. could come from an Auth object or some other object, such as a Config object. That is up to you. 

$user = new \Vespula\User\User('admin');
$user->setUsername('juser');
$user->setFullname('Joe User');

if ($acl->isAllowed($user, 'posts', 'add')) {
    // etc.
}

echo "Hello " . $user->getFullname();

```