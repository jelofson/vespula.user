<?php
namespace Vespula\User;

use Laminas\Permissions\Acl\Role\RoleInterface;
use Laminas\Permissions\Acl\ProprietaryInterface;

/**
 * A simple class for getting and setting a Laminas ACL Role ID for Laminas ACL queries
 *
 * You can also use it to hold other miscellaneous user data
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
class User implements RoleInterface, ProprietaryInterface
{
    /**
     *
     * @var string the role id 
     */
    protected $role_id;

    /**
     *
     * @var string the owner id
     * @see ProprietaryInterface
     */
    protected $owner_id;

    
    /**
     *
     * @var array An array of userdata accessible via getSomeKey() method
     * @see __call() 
     */
    protected $userdata = [];

    /**
     * Constructor. Create new User
     * 
     * @param string $role_id
     * @param mixed $owner_id
     */
    public function __construct(string $owner_id = null, string $role_id = null)
    {
        if ($owner_id) {
            $this->setOwnerId($owner_id);
        }

        if ($role_id) {
            $this->setRoleId($role_id);
        }
    }

    /**
     * Set the Role Id
     * 
     * @param string $role_id
     */
    public function setRoleId(string $role_id)
    {
        $this->role_id = $role_id;
    }

    /**
     * Get the role id
     * 
     * @return string
     */
    public function getRoleId(): string
    {
        return $this->role_id;
    }

    /**
     * Set the Owner Id
     * 
     * @param mixed $id
     */
    public function setOwnerId(string $owner_id)
    {
        $this->owner_id = $owner_id;
    }

    /**
     * Get the owner id
     * 
     * @return mixed
     */
    public function getOwnerId(): string
    {
        return $this->owner_id;
    }

    /**
     * Magic method to get and set misc userdata. For example:
     * 
     * <code>
     * $user->setFullname('Joe User');
     * $user->getFullname();
     * </code>
     * 
     * @param string $method
     * @param array $params
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function __call($method, $params)
    {
        $prefix = substr($method, 0, 3);
        if ($prefix !== 'get' && $prefix !== 'set') {
            throw new \InvalidArgumentException();
        }

        $key = lcfirst(substr($method, 3));

        if ($prefix == 'get') {
            if (array_key_exists($key, $this->userdata)) {
                return $this->userdata[$key];
            }
            trigger_error('Undefined property: ' . $key);
            return null;

        }

        if ($prefix == 'set') {
            $value = $params[0] ?? null;

            $this->userdata[$key] = $value;

        }
    }



}
