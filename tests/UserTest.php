<?php

namespace Vespula\User;

use Vespula\User\User;

class TextTest extends \PHPUnit\Framework\TestCase
{
    public function testConstruct()
    {
        $user = new User('juser', 'guest');
        $expected = 'guest';
        $actual = $user->getRoleId();
        $this->assertEquals($expected, $actual);

        $expected = 'juser';
        $actual = $user->getOwnerId();
        $this->assertEquals($expected, $actual);
    }

    public function testSetGetRoleId()
    {
        $user = new User();
        $user->setRoleId('guest');
        $expected = 'guest';
        $actual = $user->getRoleId();
        $this->assertEquals($expected, $actual);
    }

    public function testSetGet()
    {
        $user = new User();
        $user->setFullname('Joe User');
        $expected = 'Joe User';
        $actual = $user->getFullname();
        $this->assertEquals($expected, $actual);
    }

    public function testSetException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $user = new User();
        $user->seFullname('Joe User');

    }

    public function testGetException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $user = new User();
        $user->gesssFullname();

    }

    public function testSetNull()
    {

        $user = new User();
        $user->setFullname();
        $this->assertNull($user->getFullname());

    }

}
